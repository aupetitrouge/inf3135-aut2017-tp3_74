#ifndef GAMEOVER_H
#define GAMEOVER_H

#include <SDL2/SDL.h>
#include "constants.h"

// --------------- //
// Data structures //
// --------------- //

enum GameOverChoice {
	GAMEOVER_NONE, 		// User has not made a choice
	GAMEOVER_QUIT, 		// User wishes to quit
	GAMEOVER_CONTINUE	// User wishes to continue using the game
};

struct GameOver {
	enum GameOverChoice choice;	// The choice of the user
	struct Spritesheet *image;	// The gameOver sprite
	SDL_Renderer *renderer;		// The renderer
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a new gameOver message.
 *
 * @param renderer	The renderer
 * @param win		An int acting as a boolean indicating if the game was won
 * @return			A pointer to the gameOver, NULL if there was an error
 */
struct GameOver *GameOver_initialize(SDL_Renderer *renderer, int win);

/**
 * Start running the gameOver message.
 *
 * @param gameOver	The gameOver message to show
 */
void GameOver_run(struct GameOver *gameOver);

/**
 * Delete the gameOver.
 *
 * @param gameOver	The gameOver message to delete
 */
void GameOver_delete(struct GameOver *gameOver);

#endif
